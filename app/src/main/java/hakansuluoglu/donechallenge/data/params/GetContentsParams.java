package hakansuluoglu.donechallenge.data.params;

import hakansuluoglu.donechallenge.presentation.util.Constant;

public class GetContentsParams {
    private int pageNumber;
     private String query;
    private String clientId;

    public GetContentsParams(int pageNumber, String query) {
        this.pageNumber = pageNumber;
        this.query = query;
        this.clientId = Constant.clientId;
    }

    @Override
    public String toString() {
        return "GetContentsParams{" +
                "pageNumber=" + pageNumber +
                ", query='" + query + '\'' +
                ", clientId='" + clientId + '\'' +
                '}';
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
