package hakansuluoglu.donechallenge.data.source;


import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.Repository;
import hakansuluoglu.donechallenge.domain.model.Content;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton public class DataRepository implements Repository {

  private NetworkDataStore NetworkDataStore;

  @Inject
  DataRepository(DataFactory DataFactory) {
    this.NetworkDataStore = DataFactory.createNetworkDataStore();
  }

  @Override
  public Single<List<Content>> getContents(GetContentsParams params) {
    return this.NetworkDataStore.getContents(params)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread());
  }

}
