package hakansuluoglu.donechallenge.data.source;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Singleton;

import hakansuluoglu.donechallenge.data.api.RetrofitServiceGenerator;
import hakansuluoglu.donechallenge.data.api.UnsplashRestClient;

@Singleton public class DataFactory {

    private RetrofitServiceGenerator retrofitServiceGenerator;
    private final Context context;

    @Inject
    DataFactory(RetrofitServiceGenerator retrofitServiceGenerator, Context context) {
        this.retrofitServiceGenerator = retrofitServiceGenerator;
        this.context = context;
    }

    public NetworkDataStore createNetworkDataStore() {
        UnsplashRestClient unsplashRestClient = new UnsplashRestClient(retrofitServiceGenerator);
        return new NetworkDataStore(unsplashRestClient,context);
    }
}
