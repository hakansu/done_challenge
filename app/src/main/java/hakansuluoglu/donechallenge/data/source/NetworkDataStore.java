package hakansuluoglu.donechallenge.data.source;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import hakansuluoglu.donechallenge.data.api.UnsplashApi;
import hakansuluoglu.donechallenge.data.api.UnsplashRestClient;
import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.model.Content;
import hakansuluoglu.donechallenge.domain.model.response.Result;
import io.reactivex.Single;


@Singleton public class NetworkDataStore {

    private final UnsplashApi unsplashApi;
    private final Context context;

    @Inject
    NetworkDataStore(UnsplashRestClient restClient,Context context){
        this.unsplashApi = restClient.getRestApi();
        this.context = context;
    }

    public Single<List<Content>> getContents(GetContentsParams params) {
        if(isInternetAvailable()) {
            return unsplashApi.getContents(params.getPageNumber(), params.getQuery(), params.getClientId()).flatMap(apiResponse -> Single.create(emitter -> {
                List<Content> contentList = new ArrayList<>();
                List<Result> list = apiResponse.getResults();
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        Content content = new Content(list.get(i).getDescription(), list.get(i).getUrls().getRegularUrl(), list.get(i).getUser().getUserName());
                        contentList.add(content);
                    }
                    emitter.onSuccess(contentList);
                } else {
                    emitter.onError(new Exception("Bir sorun var"));
                }
             }));
        }else{
            return Single.error(new Exception("İnternet Bağlantınızı Kontrol Ediniz"));
        }
    }



    protected boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectivityManager != null) {
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        boolean ret = false;
        if (networkInfo != null) {

            if (networkInfo.isConnectedOrConnecting()) {
                ret = true;
            }
        }
        return ret;
    }

}
