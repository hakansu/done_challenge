package hakansuluoglu.donechallenge.data.api;

import hakansuluoglu.donechallenge.domain.model.response.ApiResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UnsplashApi {

  @GET("/search/photos") Single<ApiResponse> getContents(@Query("page") int page, @Query("query") String Query, @Query("client_id") String client_id);

}
