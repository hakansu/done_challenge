package hakansuluoglu.donechallenge.data.api;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Retrofit;

@Singleton public final class RetrofitServiceGenerator {

    private final Retrofit retrofit;

    @Inject public RetrofitServiceGenerator(Retrofit retrofit) {
        this.retrofit = retrofit;
    }

    <S> S createService(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }

    public Retrofit getRetrofit() {
        return this.retrofit;
    }
}
