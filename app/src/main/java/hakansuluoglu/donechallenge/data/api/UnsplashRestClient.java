package hakansuluoglu.donechallenge.data.api;

import retrofit2.Retrofit;

public class UnsplashRestClient  {
    private final RetrofitServiceGenerator retrofitServiceGenerator;

    public UnsplashRestClient(RetrofitServiceGenerator retrofitServiceGenerator) {
        this.retrofitServiceGenerator = retrofitServiceGenerator;
    }

    public UnsplashApi getRestApi() {
        return retrofitServiceGenerator.createService(UnsplashApi.class);
    }

    public Retrofit getRetrofit() {
        return retrofitServiceGenerator.getRetrofit();
    }
}
