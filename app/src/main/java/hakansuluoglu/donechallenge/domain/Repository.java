package hakansuluoglu.donechallenge.domain;

import java.util.List;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.model.Content;
import io.reactivex.Single;

public interface Repository {

    Single<List<Content>> getContents(GetContentsParams params);
}
