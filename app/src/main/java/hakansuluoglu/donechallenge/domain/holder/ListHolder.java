package hakansuluoglu.donechallenge.domain.holder;

import java.util.List;

import javax.inject.Singleton;

import hakansuluoglu.donechallenge.domain.model.Content;

@Singleton  public class ListHolder {
    private List<Content> contents;
    private String queryName;

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }
}
