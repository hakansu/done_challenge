package hakansuluoglu.donechallenge.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Content implements Parcelable {

   private String description;
   private String url;
   private String username;

    public Content(String description, String url, String username) {
        this.description = description;
        this.url = url;
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.description);
        dest.writeString(this.url);
        dest.writeString(this.username);
    }

    protected Content(Parcel in) {
        this.description = in.readString();
        this.url = in.readString();
        this.username = in.readString();
    }

    public static final Parcelable.Creator<Content> CREATOR = new Parcelable.Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel source) {
            return new Content(source);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };
}
