package hakansuluoglu.donechallenge.domain.model.response;

import com.google.gson.annotations.SerializedName;

public class Url {
    @SerializedName("regular") private String regularUrl;

    public String getRegularUrl() {
        return regularUrl;
    }

    public void setRegularUrl(String regularUrl) {
        this.regularUrl = regularUrl;
    }
}
