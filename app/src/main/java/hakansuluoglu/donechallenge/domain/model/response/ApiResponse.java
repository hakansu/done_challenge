package hakansuluoglu.donechallenge.domain.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse {

    @SerializedName("total") private int totalQuery;
    @SerializedName("total_pages") private int pageCount;
    @SerializedName("results")private List<Result> results;

    public int getTotalQuery() {
        return totalQuery;
    }

    public void setTotalQuery(int totalQuery) {
        this.totalQuery = totalQuery;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}

