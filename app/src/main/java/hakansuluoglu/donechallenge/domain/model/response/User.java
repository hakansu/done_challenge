package hakansuluoglu.donechallenge.domain.model.response;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("username") private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
