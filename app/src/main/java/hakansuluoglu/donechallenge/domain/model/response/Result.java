package hakansuluoglu.donechallenge.domain.model.response;

import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("description")private String description;
    @SerializedName("urls") private Url urls;
    @SerializedName("user")private User user;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Url getUrls() {
        return urls;
    }

    public void setUrls(Url urls) {
        this.urls = urls;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
