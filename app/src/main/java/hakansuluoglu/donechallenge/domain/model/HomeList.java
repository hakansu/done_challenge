package hakansuluoglu.donechallenge.domain.model;

import java.util.List;

public class HomeList {
    public static final int SLIDER = 0;
    public static final int HORIZONTAL_LIST = 1;

    private String query;
    private int listType;
    private int imageCount;
    private List<Content> contents;

    public HomeList(String query, int listType, int imageCount, List<Content> contents) {
        this.query = query;
        this.listType = listType;
        this.imageCount = imageCount;
        this.contents = contents;
    }

    public HomeList(String query, int listType, int imageCount) {
        this.query = query;
        this.listType = listType;
        this.imageCount = imageCount;
    }

    public int getListType() {
        return listType;
    }

    public void setListType(int listType) {
        this.listType = listType;
    }

    public int getImageCount() {
        return imageCount;
    }

    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }

    public List<Content> getContents() {
        return contents;
    }

    public void setContents(List<Content> contents) {
        this.contents = contents;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
