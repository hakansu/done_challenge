package hakansuluoglu.donechallenge.presentation.features.home;

import android.util.Log;

import org.json.JSONObject;

import java.util.List;

import javax.inject.Inject;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.Repository;
import hakansuluoglu.donechallenge.domain.model.Content;
import io.reactivex.observers.DisposableSingleObserver;
import okhttp3.ResponseBody;
import retrofit2.HttpException;

public class HomePresenter implements HomeContract.Presenter{

    private HomeContract.View homeView;
    private final Repository repository;
    private int index;

   @Inject
   public HomePresenter(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void setView(HomeContract.View view) {
        this.homeView = view;
    }

    @Override
    public void getContents(GetContentsParams params,int index) {
        repository.getContents(params).subscribeWith(new GetContentsObserver());
        this.index = index;
    }


    private final class GetContentsObserver extends DisposableSingleObserver<List<Content>> {
        @Override
        public void onSuccess(List<Content> contents) {
            HomePresenter.this.homeView.bindList(contents,index);

            if(index < HomePresenter.this.homeView.getHomeListSize() -1){
                HomePresenter.this.homeView.addList(index +1);
            }else{
                HomePresenter.this.homeView.dismissProgressBar();
            }
        }

        @Override
        public void onError(Throwable e) {
            Log.d("HATA",e.getLocalizedMessage());
                HomePresenter.this.homeView.showSnackBar(e.getMessage());
        }
    }


}
