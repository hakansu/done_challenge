package hakansuluoglu.donechallenge.presentation.features.home;

import java.util.List;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.model.Content;

public class HomeContract {

    interface View {
        void bindList(List<Content> contents,int index);
        int getHomeListSize();
        void addList(int i);
        void dismissProgressBar();
        void showSnackBar(String message);
    }

    interface Presenter{
        void setView(HomeContract.View view);
        void getContents(GetContentsParams params,int index);
    }
}
