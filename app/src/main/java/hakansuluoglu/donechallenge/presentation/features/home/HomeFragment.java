package hakansuluoglu.donechallenge.presentation.features.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.holder.ListHolder;
import hakansuluoglu.donechallenge.domain.model.Content;
import hakansuluoglu.donechallenge.domain.model.HomeList;
import hakansuluoglu.donechallenge.presentation.MainActivity;
import hakansuluoglu.donechallenge.presentation.features.adapters.HomeListAdapter;
import hakansuluoglu.donechallenge.presentation.features.endless.EndlessFragment;

public class HomeFragment extends Fragment implements HomeContract.View {

    @Inject HomePresenter homePresenter;
    @Inject ListHolder listHolder;
    private HomeListAdapter homeListAdapter;
    List<HomeList> homeLists = new ArrayList<>();

    @BindView(R.id.rv_home) RecyclerView rvHome;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    public HomeFragment() {
        setRetainInstance(true);
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        homeLists.add(new HomeList("Cat",HomeList.SLIDER,5));
        homeLists.add(new HomeList("Dog",HomeList.HORIZONTAL_LIST,10));
        homeLists.add(new HomeList("Rabbit",HomeList.HORIZONTAL_LIST,10));
        homeLists.add(new HomeList("Bird",HomeList.HORIZONTAL_LIST,10));
        //homeLists.add(new HomeList("Horse",HomeList.HORIZONTAL_LIST,10));

    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        ((MainActivity)getActivity()).getComponent().inject(this);
        addList(0);
        setupRecyclerAdapter();
        return rootView;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.homePresenter.setView(this);
    }

    public void addList(int i){
            String query = homeLists.get(i).getQuery();
            GetContentsParams params = new GetContentsParams(1,query);
            homePresenter.getContents(params,i);
    }

    private void setupRecyclerAdapter() {
        homeListAdapter = new HomeListAdapter(position -> {
            EndlessFragment endlessFragment = EndlessFragment.newInstance();
            listHolder.setContents(homeLists.get(position).getContents());
            listHolder.setQueryName(homeLists.get(position).getQuery());
            ((MainActivity)getActivity()).putFragment(endlessFragment,endlessFragment.getClass().getSimpleName(),true);
        });

        this.rvHome.setLayoutManager(new LinearLayoutManager(getContext()));
        this.rvHome.setAdapter(homeListAdapter);
    }

    @Override
    public void bindList(List<Content> contents,int index) {
         homeLists.get(index).setContents(contents);
         homeListAdapter.addList(homeLists.get(index));
         homeListAdapter.notifyDataSetChanged();
    }

    @Override
    public int getHomeListSize() {
        return homeLists.size();
    }

    @Override
    public void dismissProgressBar(){
        progressBar.setVisibility(View.GONE);
    }


    public void showSnackBar(String message){
        Snackbar.make(getActivity().findViewById(android.R.id.content),message,Snackbar.LENGTH_LONG).show();
    }

}
