package hakansuluoglu.donechallenge.presentation.features.endless;

import java.util.List;

import javax.inject.Inject;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.Repository;
import hakansuluoglu.donechallenge.domain.model.Content;
import io.reactivex.observers.DisposableSingleObserver;

public class EndlessPresenter implements EndlessContract.Presenter {


    private EndlessContract.View homeView;
    private final Repository repository;

    @Inject
    public EndlessPresenter(Repository repository) {
        this.repository = repository;
    }

    @Override
    public void setView(EndlessContract.View view) {
        this.homeView = view;
    }

    @Override
    public void getContents(GetContentsParams params) {
        repository.getContents(params).subscribeWith(new EndlessPresenter.GetContentsObserver());
    }


    private final class GetContentsObserver extends DisposableSingleObserver<List<Content>> {
        @Override
        public void onSuccess(List<Content> contents) {
            EndlessPresenter.this.homeView.bindList(contents);
        }

        @Override
        public void onError(Throwable e) {
            EndlessPresenter.this.homeView.showSnackBar(e.getMessage());
        }
    }
}
