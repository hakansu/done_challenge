package hakansuluoglu.donechallenge.presentation.features.listener;

public interface ClickListener {

    void onPositionClicked(int position);

}
