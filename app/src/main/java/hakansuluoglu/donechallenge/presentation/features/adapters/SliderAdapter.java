package hakansuluoglu.donechallenge.presentation.features.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.domain.model.Content;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class SliderAdapter extends PagerAdapter{


    private Context context;
    private List<Content> contents;

    public SliderAdapter(Context context, List<Content> contents) {
        this.context = context;
        this.contents = contents;
    }

    @Override
    public int getCount() {
        return contents.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adapter_slider_list_item, container,false);

        ImageView photo =  view.findViewById(R.id.img_slider);
        Picasso.get().load(contents.get(position).getUrl()).fit().centerCrop().transform(new RoundedCornersTransformation(20,0)).into(photo);
        container.addView(view);

        return view;
    }

    public void setData(Collection<Content> contents) {
        this.contents.clear();
        this.contents.addAll(contents);
        notifyDataSetChanged();
    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
         container.removeView((View) object);
    }
}
