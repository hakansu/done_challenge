package hakansuluoglu.donechallenge.presentation.features.adapters;

import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.domain.model.Content;
import hakansuluoglu.donechallenge.domain.model.HomeList;
import hakansuluoglu.donechallenge.presentation.features.listener.ClickListener;

public class HomeListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<HomeList> homeLists;
    private final ClickListener listener;


    @Inject
    public HomeListAdapter(ClickListener listener) {
        homeLists = new ArrayList<>();
        this.listener = listener;
    }

    public void addList(HomeList homeList){
        homeLists.add(homeList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if(viewType == HomeList.SLIDER ){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_slider_list, parent, false);
            return new SliderViewHolder(view);
        }if(viewType == HomeList.HORIZONTAL_LIST){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_horizontal_list, parent, false);
            return new HorizontalListViewHolder(view,listener);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        HomeList homeList = homeLists.get(position);
        List<Content> subList;

        if(homeList.getContents().size()>= homeList.getImageCount())
            subList = homeList.getContents().subList(0,homeList.getImageCount());
        else
            subList = homeList.getContents();

        switch (homeList.getListType()){
            case HomeList.SLIDER:
                SliderAdapter sliderAdapter = new SliderAdapter(viewHolder.itemView.getContext(),subList);
                ((SliderViewHolder) viewHolder).vpSlider.setAdapter(sliderAdapter);
                ((SliderViewHolder) viewHolder).txtQueryName.setText(homeList.getQuery());
                break;
            case HomeList.HORIZONTAL_LIST:
                HorizontalListAdapter horizontalListAdapter = new HorizontalListAdapter(subList);
                ((HorizontalListViewHolder) viewHolder).rvHorizontal.setAdapter(horizontalListAdapter);
                ((HorizontalListViewHolder) viewHolder).txtQueryName.setText(homeList.getQuery());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return homeLists.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(homeLists.get(position).getListType() == HomeList.HORIZONTAL_LIST ){
            return HomeList.HORIZONTAL_LIST;
        }else if(homeLists.get(position).getListType() == HomeList.SLIDER){
            return HomeList.SLIDER;
        }
        return -1;
    }

    public static class SliderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_query_name) TextView txtQueryName;
        @BindView(R.id.vp_slider) ViewPager vpSlider;
        @BindView(R.id.indicator)TabLayout indicator;


         SliderViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            indicator.setupWithViewPager(vpSlider,true);

        }
    }

    public static class HorizontalListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_query_name) TextView txtQueryName;
        @BindView(R.id.txt_more) TextView txtMore;
        @BindView(R.id.rv_horizontal) RecyclerView rvHorizontal;
        private WeakReference<ClickListener> listenerRef;

        @OnClick(R.id.txt_more) void onClickMore(){
            listenerRef.get().onPositionClicked(getAdapterPosition());
        }

        public HorizontalListViewHolder(View v,ClickListener listener) {
            super(v);
            ButterKnife.bind(this, v);
            rvHorizontal.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            listenerRef = new WeakReference<>(listener);
        }

    }
}
