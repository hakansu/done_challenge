package hakansuluoglu.donechallenge.presentation.features.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.domain.model.Content;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class EndlessAdapter extends BaseAdapter {

    private Context context;
    private List<Content> contents;

    public EndlessAdapter(Context context, List<Content> contents) {
        this.context = context;
        this.contents = contents;
    }

    public void addAll(List<Content> contents){
        this.contents.addAll(contents);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return contents.size();
    }

    @Override
    public Object getItem(int i) {
        return contents.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        EndlessViewHolder holder;
        if (view == null) {
            view = inflater.inflate(R.layout.adapter_horizontal_list_item, viewGroup, false);
            holder = new EndlessViewHolder(view);
            view.setTag(holder);
        }else {
            holder = (EndlessViewHolder) view.getTag();
        }

        Content content = contents.get(position);
        holder.txtDescription.setText(content.getDescription());
        holder.txtUsername.setText(content.getUsername());
        Picasso.get().load(content.getUrl()).fit().centerCrop().transform(new RoundedCornersTransformation(20,0)).into(holder.imgHorizontal);

        return view;
    }


    public static class EndlessViewHolder  {

        @BindView(R.id.img_horizontal) ImageView imgHorizontal;
        @BindView(R.id.txt_descripton) TextView txtDescription;
        @BindView(R.id.txt_username) TextView txtUsername;

        public EndlessViewHolder(View v) {
            ButterKnife.bind(this, v);
        }

    }
}
