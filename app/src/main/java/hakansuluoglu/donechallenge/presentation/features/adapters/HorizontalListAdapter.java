package hakansuluoglu.donechallenge.presentation.features.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.domain.model.Content;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.ContentViewHolder> {

    private List<Content> contents;


    public HorizontalListAdapter(List<Content> contents) {
        this.contents = contents;
    }

    @NonNull
    @Override
    public ContentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_horizontal_list_item, parent, false);
        return new ContentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContentViewHolder holder, int position) {
       Content content =  contents.get(position);


       holder.txtDescription.setText(content.getDescription());
       holder.txtUsername.setText(content.getUsername());
       Picasso.get().load(content.getUrl()).fit().centerCrop().transform(new RoundedCornersTransformation(20,0)).into(holder.imgHorizontal);

    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public void setData(Collection<Content> contents) {
        this.contents.clear();
        this.contents.addAll(contents);
        notifyDataSetChanged();
    }

    public static class ContentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_horizontal) ImageView imgHorizontal;
        @BindView(R.id.txt_descripton) TextView txtDescription;
        @BindView(R.id.txt_username) TextView txtUsername;


        public ContentViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }


    }
}
