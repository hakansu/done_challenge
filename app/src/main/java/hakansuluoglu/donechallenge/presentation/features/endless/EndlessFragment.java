package hakansuluoglu.donechallenge.presentation.features.endless;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.holder.ListHolder;
import hakansuluoglu.donechallenge.domain.model.Content;
import hakansuluoglu.donechallenge.presentation.MainActivity;
import hakansuluoglu.donechallenge.presentation.features.adapters.EndlessAdapter;

public class EndlessFragment extends Fragment implements EndlessContract.View{

    @Inject EndlessPresenter endlessPresenter;
    @Inject ListHolder listHolder;
    private EndlessAdapter endlessAdapter;
    @BindView(R.id.gv_contents) GridView gvContents;
    @BindView(R.id.txt_query_name)TextView txtQueryName;
    public int pageNumber = 2;

    public EndlessFragment() {
        setRetainInstance(true);
    }

    public static EndlessFragment newInstance() {
        return new EndlessFragment();
    }

    @Override public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_endless, container, false);
        ButterKnife.bind(this, rootView);
        ((MainActivity)getActivity()).getComponent().inject(this);
        txtQueryName.setText(listHolder.getQueryName());

        endlessAdapter = new EndlessAdapter(getContext(),listHolder.getContents());
        gvContents.setAdapter(endlessAdapter);

        gvContents.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }
            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (gvContents.getLastVisiblePosition() + 1 == totalItemCount ) {
                    GetContentsParams params = new GetContentsParams(pageNumber,listHolder.getQueryName());
                    endlessPresenter.getContents(params);
                    pageNumber++;
            } }});

        return rootView;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.endlessPresenter.setView(this);
    }

    @Override
    public void bindList(List<Content> contents) {
        endlessAdapter.addAll(contents);
    }

    @Override
    public void showSnackBar(String message){
        Snackbar.make(getActivity().findViewById(android.R.id.content),message,Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
