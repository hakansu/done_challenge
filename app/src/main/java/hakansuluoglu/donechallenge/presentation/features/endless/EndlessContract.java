package hakansuluoglu.donechallenge.presentation.features.endless;

import java.util.List;

import hakansuluoglu.donechallenge.data.params.GetContentsParams;
import hakansuluoglu.donechallenge.domain.model.Content;

public class EndlessContract {
    interface View {
        void bindList(List<Content> contents);
        void showSnackBar(String message);
    }

    interface Presenter{
        void setView(EndlessContract.View view);
        void getContents(GetContentsParams params);
    }

}
