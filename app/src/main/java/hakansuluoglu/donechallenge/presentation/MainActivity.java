package hakansuluoglu.donechallenge.presentation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import hakansuluoglu.donechallenge.App;
import hakansuluoglu.donechallenge.R;
import hakansuluoglu.donechallenge.presentation.di.component.ActivityComponent;
import hakansuluoglu.donechallenge.presentation.di.component.ApplicationComponent;
import hakansuluoglu.donechallenge.presentation.di.component.DaggerActivityComponent;
import hakansuluoglu.donechallenge.presentation.di.module.ActivityModule;
import hakansuluoglu.donechallenge.presentation.features.home.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityComponent activityComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.activityComponent =
                DaggerActivityComponent.builder().applicationComponent(getApplicationComponent()).activityModule(getActivityModule()).build();

        HomeFragment homeFragment = HomeFragment.newInstance();
        putFragment(homeFragment,homeFragment.getClass().getSimpleName(),false);
    }


    public ApplicationComponent getApplicationComponent() {
        return ((App)getApplication()).getApplicationComponent();
    }

    public ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    public ActivityComponent getComponent() {
        return this.activityComponent;
    }

    public void putFragment( Fragment fragment, String tagName, boolean addToStack) {
        if (addToStack) {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment, tagName).addToBackStack(tagName).commitAllowingStateLoss();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment, tagName).commitAllowingStateLoss();
        }
    }

}
