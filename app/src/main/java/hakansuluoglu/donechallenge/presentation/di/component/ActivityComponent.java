package hakansuluoglu.donechallenge.presentation.di.component;

import android.app.Activity;

import dagger.Component;
import hakansuluoglu.donechallenge.presentation.di.module.ActivityModule;
import hakansuluoglu.donechallenge.presentation.di.scope.PerActivity;
import hakansuluoglu.donechallenge.presentation.features.endless.EndlessFragment;
import hakansuluoglu.donechallenge.presentation.features.home.HomeFragment;

@PerActivity
@Component(modules = ActivityModule.class, dependencies = ApplicationComponent.class) public interface ActivityComponent {

  Activity activity();

  void inject(HomeFragment homeFragment);
  void inject(EndlessFragment endlessFragment);


}
