package hakansuluoglu.donechallenge.presentation.di.module;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import hakansuluoglu.donechallenge.presentation.di.scope.PerActivity;

@Module
public class ActivityModule {

    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @PerActivity
    @Provides
    Activity activity() {
        return this.activity;
    }

}
