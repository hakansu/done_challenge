package hakansuluoglu.donechallenge.presentation.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hakansuluoglu.donechallenge.App;
import hakansuluoglu.donechallenge.data.source.DataRepository;
import hakansuluoglu.donechallenge.domain.Repository;
import hakansuluoglu.donechallenge.domain.holder.ListHolder;

@Module  public class ApplicationModule {

    private final App application;

    public ApplicationModule(App application) {
        this.application = application;
    }

    @Provides @Singleton
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides @Singleton
    Repository Repository(DataRepository dataRepository) {
        return dataRepository;
    }

    @Provides @Singleton
    ListHolder provideListGHolder(){
        return new ListHolder();
    }

}
