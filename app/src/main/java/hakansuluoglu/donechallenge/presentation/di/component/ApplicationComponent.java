package hakansuluoglu.donechallenge.presentation.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import hakansuluoglu.donechallenge.App;
import hakansuluoglu.donechallenge.data.api.RetrofitServiceGenerator;
import hakansuluoglu.donechallenge.data.api.UnsplashRestClient;
import hakansuluoglu.donechallenge.data.source.DataFactory;
import hakansuluoglu.donechallenge.domain.Repository;
import hakansuluoglu.donechallenge.domain.holder.ListHolder;
import hakansuluoglu.donechallenge.presentation.di.module.ApplicationModule;
import hakansuluoglu.donechallenge.presentation.di.module.NetworkModule;

@Singleton @Component (modules = { ApplicationModule.class , NetworkModule.class})
public interface ApplicationComponent {

    void inject(App app);

    RetrofitServiceGenerator retrofitServiceGenerator();

    Repository repository();

    UnsplashRestClient unsplashApiRestClient();

    DataFactory dataFactory();

    Context context();

    ListHolder listHolder();
}
