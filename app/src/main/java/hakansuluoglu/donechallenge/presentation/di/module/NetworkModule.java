package hakansuluoglu.donechallenge.presentation.di.module;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hakansuluoglu.donechallenge.BuildConfig;
import hakansuluoglu.donechallenge.data.api.RetrofitServiceGenerator;
import hakansuluoglu.donechallenge.data.api.UnsplashRestClient;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module public class NetworkModule {

    @Provides @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    @Provides @Singleton
    OkHttpClient provideOkhttpClient() {
        return new OkHttpClient().newBuilder().build();
    }

    @Provides @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides @Singleton
    RetrofitServiceGenerator provideRetrofitServiceGenerator(Retrofit retrofit) {
        return new RetrofitServiceGenerator(retrofit);
    }

    @Provides @Singleton
    UnsplashRestClient provideRestClient(RetrofitServiceGenerator retrofitServiceGenerator) {
        return new UnsplashRestClient(retrofitServiceGenerator);
    }

}
