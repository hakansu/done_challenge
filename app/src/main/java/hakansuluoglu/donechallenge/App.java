package hakansuluoglu.donechallenge;

import android.app.Application;

import hakansuluoglu.donechallenge.presentation.di.component.ApplicationComponent;
import hakansuluoglu.donechallenge.presentation.di.component.DaggerApplicationComponent;
import hakansuluoglu.donechallenge.presentation.di.module.ApplicationModule;
import hakansuluoglu.donechallenge.presentation.di.module.NetworkModule;

public class App extends Application{

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initializeInjector();
    }

    private void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();

        this.applicationComponent.inject(this);
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }
}
